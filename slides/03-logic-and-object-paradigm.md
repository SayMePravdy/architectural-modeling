# Системная инженерия

## Архитектурное моделирование компьютерных систем

## Лекция 3

## Логическая и объектная парадигма

Пенской А.В., 2022

----

## План лекции

- Проблемы субстанциональной парадигмы
- Логическая парадигма
- Объектная парадигма
- Практическое использование

---

## Проблемы субстанциональной парадигмы

- Как описать процессы? (пока пропустим)
- Как определить субстанцию?
- Как описать отношения между атрибутами?

----

### Как определить субстанцию

![](figures/boro-substance-primary-substance-or-attribute-option.png)

----

![](figures/boro-substance-primary-substance-or-attribute.png)

Субстанция -- позволяет "схлопнуть" объекты реального мира на уровне концепций.

----

### Как описать отношения между атрибутами

![](figures/boro-indidual-relation-by-attribute.png)

Отношения между объектами "встраиваются" в их субстанцию.

---

## Логическая парадигма

### (Logical paradigm, extensionalism)

![](figures/george-boole.jpg)
![](figures/john-venn.jpg)
![](figures/georg-cantor.jpg)

- Логический класс
- Логическая пара (tuple)
- Конкретный объект (individual)

----

*Disclaimer*:

- Как "кодировать" -- за скобками.

----

### Соответствие объекта и концепта (знака)

![](figures/boro-logic-one-obj-many-concepts.png)

- Один объект и много концептов -- норма.
- Что такое "идентичность" объекта?
- "Противоречие? Разрешите!" (с)

----

### От субстанции в расширениям (extension)

- Идентичность -- не свойство объекта.
- Идентичность -- свойство наблюдателя.
- Любое свойство объекта -- внешнее.

<div>

![](figures/boro-logic-car-from-substance.png)

- Как различить одинаковые машины, если не по координатам?
- Разве координата имеет отношение к субстанции?
- Принцип строгого соответствия.

</div> <!-- .element: class="fragment" -->

----

### Атрибут как расширение (extension)

![](figures/boro-logic-red-things.png)

----

### Субстанция и атрибуты как расширения

![](figures/boro-logic-color-things.png)

----

### Множественная классификация

![](figures/boro-logic-multi-classification.png)

----

### Минимизация модели

![](figures/boro-logic-classes-minimisation.png)

- Управление гранулярностью.

---

### Отношение между объектами как пара (tuple, кортеж)

<div class="row"><div class="col">

![](figures/boro-indidual-relation-by-attribute.png)

</div><div class="col">

![](figures/boro-logic-relations.png)

</div></div>

----

### Идентичность пары. Повторное использование

![](figures/boro-logic-relations-many.png)

----

### Отношение многие ко многим. Компрессия

![](figures/boro-logic-relations-many-to-many.png)

----

### Отношение абстрактного и конкретного

#### Подкласс

![](figures/boro-logic-color-subclass.png)

----

#### Класс классов

![](figures/boro-logic-class-of-colors.png)

----

![](figures/py-instance-class-metaclass.png)<!-- .element: height="100px" -->

(черновик)

<div class="row"><div class="col">

```python
object.__class__ # -> <class 'type'>

class Colors(type): pass;
Colors.__class__ # -> <class 'type'>
isinstance(Colors, type) # -> True

class ColorObjects(object): pass
class RedObjects(ColorObjects, 
                 metaclass=Colors):
    pass

RedObjects.__class__ # -> <class '__main__.Colors'>
RedObjects.__class__.__class__ # -> <class 'type'>
isinstance(RedObjects, type) # -> True
isinstance(RedObjects, Colors) # -> True
```

</div><div class="col">

```python
class Cicle(object): pass;

class A(RedObjects, Cicle): pass;

isinstance(A, ColorObjects) # -> True
isinstance(A, RedObjects) # -> False
isinstance(A, Cicle) # -> False
A.__class__ # -> <class '__main__.ColorObjects'>
A.__class__.__class__ # -> <class 'type'>

a = A()
isinstance(a, A) # -> True
isinstance(a, Cicle) # -> True
isinstance(a, RedObjects) # -> True
isinstance(a, ColorObjects) # -> True
isinstance(a, Colors) # -> False

a.__class__
a.__class__.__class__
```

</div></div>

---

## Логическая парадигма. Summary

### Субстанция и атрибуты

![](figures/boro-logic-summary-non-relation-attribute.png)

----

### Отношения

![](figures/boro-logic-summary-relation.png)

----

### Отображение парадигм

![](figures/boro-logic-summary.png)

---

## Объектная парадигма

### (Object paradigm, 4d extensionalism)

![](figures/chris-partridge.jpeg)

- Время.
- Объекты всего жизненного цикла (whole life individual).
- Темпоральные части (temporal parts).

----

### Процесс. Динамические расширения

![](figures/boro-object-dynamic-extension.png)

----

### Эксперимент с машиной

![](figures/boro-object-car-experiment.png)

В чём проблема и как с ней бороться?

----

### Реальные объекты существуют во времени

![](figures/boro-object-four-dimensional-lisa-brown.png)

----

### Эксперимент с машиной во времени

![](figures/boro-object-car-experiment-2.png)

----

### Четырёхмерная бабочка

<div class="row"><div class="col">

![](figures/boro-object-butterfly-old.png)

</div><div class="col">

![](figures/boro-object-butterfly-new.png)

</div></div>

----

### Выход на службу нового директора

<div class="row"><div class="col">

![](figures/boro-object-banck-chairman-change-view.png)

</div><div class="col">

![](figures/boro-object-banck-chairman-change.png)

</div></div>

----

### Отношения между объектами во времени

![](figures/boro-object-relation-in-time.png)

---

## Практическое использование

### Теория. Высшие онтологии

<div class="row"><div class="col">

Upper ontology — is an ontology (in the sense used in information science) which describes very general concepts that are the same across all knowledge domains.

Включает (открытый список):

- “Философские категории” (реальное — абстрактное, естественное — техническое и др.).
- Отношение части и целого.
- Пространство и время.

</div><div class="col">

![](figures/babel-fish.png)

</div></div>

----

### Практика

<div class="row"><div class="col">

- Интеграция данных. National Institute of Standards and Technology от 2004 года: до 40 процентов инженерного времени тратится на поиск и верификацию данных. Расходы в США до 15 миллиардов в год.
    - ISO 15926, IDEAS.

- Программная инженерия (BORO Method).

- CSS — Cascading Style Sheets.

</div><div class="col">

![](figures/iec-81346-coding-tech.png)

</div></div>

----

#### ISO-15926 Top level classes

Предположения?

<div class="row"><div class="col">

<div>

![](figures/gp-things.png)

(подсказка)

</div> <!-- .element: class="fragment" -->

</div><div class="col">

<div>

![](figures/iso-15926-top-classes.png)

(ответ)

</div> <!-- .element: class="fragment" -->

</div></div>

----

#### ISO-15926 Замена насоса

<div class="row"><div class="col">

![](figures/iso-15926-pump-change.png)

</div><div class="col">

![](figures/iso-15926-pump-change-ontology.png)

</div></div>

----

#### ISO-15926 Application ontology example

![](figures/iso-15926-ontology.png)

----

#### ISO-15926 Template example

![](figures/iso-15926-template.png)
